#CHALLANGE 1.1
#The clients who had cancelled orders (fields to be extracted: client name, country, city)
SELECT 
customerName,
country,
city
FROM classicmodels.customers c
JOIN classicmodels.orders o on o.customerNumber=c.customerNumber
JOIN classicmodels.orderdetails od on od.orderNumber=o.orderNumber
WHERE o.status='Cancelled'
GROUP BY 
customerName,
country,
city;



#CHALLANGE 1.2
#Total orders which were not cancelled, split per year and month (fields to be extracted:
#year, month, orders);

SELECT 
YEAR(orderDate) year,
MONTH(orderDate) month,
COUNT(orderNumber) orders_count
FROM classicmodels.orders o
WHERE o.status !='Cancelled'
GROUP BY 
YEAR(orderDate),
MONTH(orderDate);

#CHALANGE 1.3
#The top 3 products for each product line with the highest inventory value (by inventory
#value we refer to the total value of the cars that are the same - fields to be extracted:
#product line, productcode, productname, total value)

SELECT COUNT(productName) duplicated_products FROM classicmodels.products
group by productCode
HAVING COUNT(productName)>1;
# There are no duplicated products, therefore no need to aggregate buyPrice.

 SELECT productLine,
		productCode,
		productName, 
        buyPrice
   FROM
     (SELECT productLine,
		productCode,
		productName, 
        buyPrice,
                  @productLine_rank := IF(@current_productLine = productLine, @productLine_rank + 1, 1) AS productLine_rank,
                  @current_productLine := productLine 
       FROM classicmodels.products
       ORDER BY productLine, buyPrice DESC
     ) ranked
   WHERE productLine_rank <= 3;
   
#CHALANGE 1.4
DELIMITER $$
CREATE DEFINER=`root`@`localhost` 
FUNCTION `employee_city`(employeeNumber int(200)) RETURNS varchar(20) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
DECLARE city varchar(20);
	SELECT 
    offices.city
INTO city FROM
    employees
        JOIN
    offices ON offices.officeCode = employees.officeCode
WHERE
    employees.employeeNumber = employeeNumber;
RETURN city;
END$$
DELIMITER ;

#e.g. use of function
select employee_city(1002);

